# Loadfile Normalizer

This tool normalizes Concordance loadfiles and allows for the development of modules with additional normalization rules.

## Build
Open the NuGet Packet manager and restore missing packages.

## Extend
To implement a new normalization module, create a new class that inherits from the abstract class NormalizationModule and implement all methods.

## Licence
  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.