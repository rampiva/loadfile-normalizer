﻿using System;
using System.Collections.Generic;
using ZetaLongPaths;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rampiva.LoadfileNormalizer.NormalizerModules
{
    public abstract class NormalizationModule
    {

        /// <summary>
        /// This method will be called once with the elements of the loadfile header
        /// </summary>
        /// <returns>
        /// A list of normalized header columns to be retained in the loadfile
        /// </returns>
        public abstract List<string> NormalizeHeader(List<string> inputHeader);

        /// <summary>
        /// This method will be called for every line in the loadfile, with the elements of each line
        /// </summary>
        /// <returns>
        /// A list of normalized elements to be retained in the loadfile.
        /// The result must be of the same size as the normalized header.
        /// </returns>
        public abstract List<string> NormalizeLine(List<string> inputLine, long lineId);


        /// <summary>
        /// This method will be called after all of the lines in the loadfile have been normalized
        /// </summary>
        /// <returns>
        /// A user message or null for no message
        /// </returns>
        public abstract string GetFinishedStatus();

        /// <returns>
        /// The author of the normalizer module
        /// </returns>
        public abstract string GetAuthor();

        /// <returns>
        /// A short description of what the module does
        /// </returns>
        public abstract string GetDescription();


        public override string ToString()
        {
            return GetDescription() + " (" + GetAuthor() + ")";
        }

        public virtual void setLoadFileFolder(ZlpDirectoryInfo di)
        { }
    }
}
