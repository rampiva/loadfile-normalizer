﻿using Rampiva.LoadfileNormalizer.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rampiva.LoadfileNormalizer.NormalizationModules
{
    public partial class FindAndReplaceForm : Form
    {
        private bool accept;
        public FindAndReplaceForm()
        {
            InitializeComponent();
            checkBoxReplaceInTextFilename.Checked = OptionsPersistance.getInstance().getBooleanOption("FindAndReplace", "ReplaceInTextFilename", false);
            checkBoxReplaceInNativeFilename.Checked = OptionsPersistance.getInstance().getBooleanOption("FindAndReplace", "ReplaceInNativeFilename", false);


            textBoxOriginalText.Text = OptionsPersistance.getInstance().getTextOption("FindAndReplace", "SearchFor", "");
            textBoxReplaceWith.Text= OptionsPersistance.getInstance().getTextOption("FindAndReplace", "ReplaceWith", "");

        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            accept = true;
            OptionsPersistance.getInstance().setBooleanOption("FindAndReplace", "ReplaceInTextFilename", checkBoxReplaceInTextFilename.Checked);
            OptionsPersistance.getInstance().setBooleanOption("FindAndReplace", "ReplaceInNativeFilename", checkBoxReplaceInNativeFilename.Checked);

            OptionsPersistance.getInstance().setTextOption("FindAndReplace", "SearchFor", textBoxOriginalText.Text);
            OptionsPersistance.getInstance().setTextOption("FindAndReplace", "ReplaceWith", textBoxReplaceWith.Text);

            this.Close();
        }

        public bool GetAccept()
        {
            return accept;
        }

        public bool GetRemoveContentNotInDateFormat()
        {
            return checkBoxReplaceInTextFilename.Checked;
        }
    }
}
