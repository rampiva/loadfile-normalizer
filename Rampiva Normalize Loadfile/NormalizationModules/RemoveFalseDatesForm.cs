﻿using Rampiva.LoadfileNormalizer.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rampiva.LoadfileNormalizer.NormalizationModules
{
    public partial class RemoveFalseDatesForm : Form
    {
        private bool accept;
        private string[] defaultDateFormats;
        public RemoveFalseDatesForm(string[] defaultDateFormats)
        {
            InitializeComponent();
            this.defaultDateFormats = defaultDateFormats;
            checkBoxRemoveContentNotInDateFormat.Checked = OptionsPersistance.getInstance().getBooleanOption("RemoveFalseDates", "RemoveContentNotInDateFormat", false);
            textBoxAllowedDateFormats.Lines = OptionsPersistance.getInstance().getMultiTextOption("RemoveFalseDates", "AllowedDateFormat", defaultDateFormats);
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            accept = true;
            OptionsPersistance.getInstance().setBooleanOption("RemoveFalseDates", "RemoveContentNotInDateFormat", checkBoxRemoveContentNotInDateFormat.Checked);
            OptionsPersistance.getInstance().setMultiTextOption("RemoveFalseDates", "AllowedDateFormat", textBoxAllowedDateFormats.Lines);                
            this.Close();
        }

        public bool GetAccept()
        {
            return accept;
        }

        public bool GetRemoveContentNotInDateFormat()
        {
            return checkBoxRemoveContentNotInDateFormat.Checked;
        }

        public string[] GetAllowedDateFormats()
        {
            return textBoxAllowedDateFormats.Lines;
        }

        private void linkLabelDateFormats_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(linkLabelDateFormats.Text);
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            textBoxAllowedDateFormats.Lines = defaultDateFormats;
        }
    }
}
