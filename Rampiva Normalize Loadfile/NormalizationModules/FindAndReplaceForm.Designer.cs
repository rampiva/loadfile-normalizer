﻿namespace Rampiva.LoadfileNormalizer.NormalizationModules
{
    partial class FindAndReplaceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FindAndReplaceForm));
            this.labelSelectDates = new System.Windows.Forms.Label();
            this.checkedListBoxDateFieldsHeaders = new System.Windows.Forms.CheckedListBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.textBoxOriginalText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelReplaceWith = new System.Windows.Forms.Label();
            this.textBoxReplaceWith = new System.Windows.Forms.TextBox();
            this.checkBoxReplaceInTextFilename = new System.Windows.Forms.CheckBox();
            this.checkBoxReplaceInNativeFilename = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // labelSelectDates
            // 
            this.labelSelectDates.AutoSize = true;
            this.labelSelectDates.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.labelSelectDates.Location = new System.Drawing.Point(12, 114);
            this.labelSelectDates.Name = "labelSelectDates";
            this.labelSelectDates.Size = new System.Drawing.Size(85, 13);
            this.labelSelectDates.TabIndex = 0;
            this.labelSelectDates.Text = "Replace in fields";
            // 
            // checkedListBoxDateFieldsHeaders
            // 
            this.checkedListBoxDateFieldsHeaders.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBoxDateFieldsHeaders.CheckOnClick = true;
            this.checkedListBoxDateFieldsHeaders.FormattingEnabled = true;
            this.checkedListBoxDateFieldsHeaders.Location = new System.Drawing.Point(12, 130);
            this.checkedListBoxDateFieldsHeaders.Name = "checkedListBoxDateFieldsHeaders";
            this.checkedListBoxDateFieldsHeaders.Size = new System.Drawing.Size(610, 574);
            this.checkedListBoxDateFieldsHeaders.TabIndex = 1;
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOK.Location = new System.Drawing.Point(547, 709);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 2;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // textBoxOriginalText
            // 
            this.textBoxOriginalText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxOriginalText.Location = new System.Drawing.Point(84, 6);
            this.textBoxOriginalText.Name = "textBoxOriginalText";
            this.textBoxOriginalText.Size = new System.Drawing.Size(174, 20);
            this.textBoxOriginalText.TabIndex = 72;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 73;
            this.label1.Text = "Search for";
            // 
            // labelReplaceWith
            // 
            this.labelReplaceWith.AutoSize = true;
            this.labelReplaceWith.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.labelReplaceWith.Location = new System.Drawing.Point(12, 35);
            this.labelReplaceWith.Name = "labelReplaceWith";
            this.labelReplaceWith.Size = new System.Drawing.Size(69, 13);
            this.labelReplaceWith.TabIndex = 74;
            this.labelReplaceWith.Text = "Replace with";
            // 
            // textBoxReplaceWith
            // 
            this.textBoxReplaceWith.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxReplaceWith.Location = new System.Drawing.Point(84, 32);
            this.textBoxReplaceWith.Name = "textBoxReplaceWith";
            this.textBoxReplaceWith.Size = new System.Drawing.Size(174, 20);
            this.textBoxReplaceWith.TabIndex = 75;
            // 
            // checkBoxReplaceInTextFilename
            // 
            this.checkBoxReplaceInTextFilename.AutoSize = true;
            this.checkBoxReplaceInTextFilename.Location = new System.Drawing.Point(15, 58);
            this.checkBoxReplaceInTextFilename.Name = "checkBoxReplaceInTextFilename";
            this.checkBoxReplaceInTextFilename.Size = new System.Drawing.Size(150, 17);
            this.checkBoxReplaceInTextFilename.TabIndex = 80;
            this.checkBoxReplaceInTextFilename.Text = "Replace in TEXT filename";
            this.checkBoxReplaceInTextFilename.UseVisualStyleBackColor = true;
            // 
            // checkBoxReplaceInNativeFilename
            // 
            this.checkBoxReplaceInNativeFilename.AutoSize = true;
            this.checkBoxReplaceInNativeFilename.Location = new System.Drawing.Point(15, 81);
            this.checkBoxReplaceInNativeFilename.Name = "checkBoxReplaceInNativeFilename";
            this.checkBoxReplaceInNativeFilename.Size = new System.Drawing.Size(161, 17);
            this.checkBoxReplaceInNativeFilename.TabIndex = 81;
            this.checkBoxReplaceInNativeFilename.Text = "Replace in NATIVE filename";
            this.checkBoxReplaceInNativeFilename.UseVisualStyleBackColor = true;
            // 
            // FindAndReplaceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 744);
            this.Controls.Add(this.checkBoxReplaceInNativeFilename);
            this.Controls.Add(this.checkBoxReplaceInTextFilename);
            this.Controls.Add(this.textBoxReplaceWith);
            this.Controls.Add(this.labelReplaceWith);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxOriginalText);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.checkedListBoxDateFieldsHeaders);
            this.Controls.Add(this.labelSelectDates);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1920, 1080);
            this.MinimumSize = new System.Drawing.Size(650, 400);
            this.Name = "FindAndReplaceForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Normlize date fields";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelSelectDates;
        public System.Windows.Forms.CheckedListBox checkedListBoxDateFieldsHeaders;
        public System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelReplaceWith;
        public System.Windows.Forms.TextBox textBoxOriginalText;
        public System.Windows.Forms.TextBox textBoxReplaceWith;
        public System.Windows.Forms.CheckBox checkBoxReplaceInTextFilename;
        public System.Windows.Forms.CheckBox checkBoxReplaceInNativeFilename;
    }
}