﻿using Rampiva.LoadfileNormalizer.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rampiva.LoadfileNormalizer.NormalizationModules
{
    public partial class ReplaceSurrogateCharactersInTextFilesForm : Form
    {
        private bool accept;
        private string textPathField;
        public ReplaceSurrogateCharactersInTextFilesForm(List<string> inputHeader)
        {
            InitializeComponent();
            comboBoxTextPathField.Items.Clear();
            comboBoxTextPathField.Items.AddRange(inputHeader.ToArray());
            textPathField = OptionsPersistance.getInstance().getTextOption("ReplaceSurrogateCharactersInTextFieles", "TextPathField","TEXTPATH");
            if (inputHeader.Contains(textPathField))
            {
                comboBoxTextPathField.SelectedItem = textPathField;
            }

            accept = false;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            accept = true;
            textPathField = comboBoxTextPathField.SelectedItem.ToString();
            OptionsPersistance.getInstance().setTextOption("ReplaceSurrogateCharactersInTextFieles", "TextPathField", textPathField);            
            this.Close();
        }


        public bool GetAccept()
        {
            return accept;
        }

        public string GetTextPathField()
        {
            return textPathField;
        }

    }
}
