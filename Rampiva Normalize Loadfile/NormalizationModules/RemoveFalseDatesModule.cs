﻿using Rampiva.LoadfileNormalizer.NormalizationModules;
using System;
using System.Collections.Generic;
using System.Linq;
using Rampiva.LoadfileNormalizer.Utils;
using System.Globalization;

namespace Rampiva.LoadfileNormalizer.NormalizerModules
{
    class RemoveFalseDatesModule : NormalizationModule
    {
        private static readonly log4net.ILog LOGGER = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private List<int> headerDateFieldLoc = new List<int>();
        private long totalDatesRemovedBeforeEpoch = 0;
        private long totalDatesRemovedAfterMaxDate = 0;
        private long totalDatesRemovedInvalidFormat = 0;
        private DateTime epoch = DateTime.Parse("01/01/1970");
        private DateTime maxDate = DateTime.Parse("01/01/2099");
        private RemoveFalseDatesForm myForm;
        private bool removeContentNotInDateFormat;
        private string[] allowedDateFormats;
        private CultureInfo cultureInfo = CultureInfo.CreateSpecificCulture("en-US");
        private long invalidDateFormatWarningsCount = 0;
        private const long MAX_INVALID_DATE_FORMAT_WARNINGS_COUNT = 1000;
        private List<string> inputHeader;

        public override List<string> NormalizeHeader(List<string> inputHeader)
        {
            this.inputHeader = inputHeader;
            string[] defaultDateFormats = new string[] { "M/d/yyyy", "M/d/yyyy h:mm:ss tt", "M/d/yyyy H:mm:ss" };
            using (myForm = new RemoveFalseDatesForm(defaultDateFormats))
            {                
                for (int a = 0; a < inputHeader.Count(); a++)
                {
                    myForm.checkedListBoxDateFieldsHeaders.Items.Add(inputHeader[a]);
                    myForm.checkedListBoxDateFieldsHeaders.SetItemChecked(myForm.checkedListBoxDateFieldsHeaders.Items.Count - 1, OptionsPersistance.getInstance().getBooleanOption("RemoveFalseDates", inputHeader[a], false));
            
                }
                myForm.ShowDialog();
                removeContentNotInDateFormat = myForm.GetRemoveContentNotInDateFormat();
                allowedDateFormats = myForm.GetAllowedDateFormats();

                if (myForm.GetAccept())
                {
                    if (myForm.checkedListBoxDateFieldsHeaders.CheckedItems.Count != 0)
                    {
                        for (int id = 0; id < myForm.checkedListBoxDateFieldsHeaders.Items.Count; id++)
                        {
                            bool isItemChecked = myForm.checkedListBoxDateFieldsHeaders.GetItemChecked(id);
                            if (isItemChecked)
                            {
                                headerDateFieldLoc.Add(id);
                            }
                            OptionsPersistance.getInstance().setBooleanOption("RemoveFalseDates", inputHeader[id], isItemChecked);
                        }
                    }
                    else
                    {
                        //Stop doing work
                    }
                }

            }
            return inputHeader;
        }

        public override List<string> NormalizeLine(List<string> inputLine, long lineId)
        {
            for (int loc = 0; loc < headerDateFieldLoc.Count(); loc++)
            {
                if (inputLine[headerDateFieldLoc[loc]].Length > 0)
                {
                    var tempDateText = inputLine[headerDateFieldLoc[loc]];
                    bool dateInValidFormat = false;
                    foreach (string allowedDateFormat in allowedDateFormats)
                    {
                        if (allowedDateFormat.Length > 0)
                        {

                            DateTime tempDate;
                            var result = DateTime.TryParseExact(tempDateText, allowedDateFormat, null, System.Globalization.DateTimeStyles.None, out tempDate);
                            if (!result)
                            {
                                continue;
                            }
                            dateInValidFormat = true;
                            if (tempDate.Date < epoch.Date)
                            {
                                inputLine[headerDateFieldLoc[loc]] = "";
                                totalDatesRemovedBeforeEpoch++;
                            }
                            else if (tempDate.Date > maxDate.Date)
                            {
                                inputLine[headerDateFieldLoc[loc]] = "";
                                totalDatesRemovedAfterMaxDate++;
                            }
                            break;

                        }
                    }

                    if (!dateInValidFormat)
                    {
                        if (removeContentNotInDateFormat)
                        {
                            inputLine[headerDateFieldLoc[loc]] = "";
                            totalDatesRemovedInvalidFormat++;
                        }
                        else
                        {
                            invalidDateFormatWarningsCount++;
                            if (invalidDateFormatWarningsCount == MAX_INVALID_DATE_FORMAT_WARNINGS_COUNT)
                            {
                                LOGGER.Error("Encountered a large number of date conversion warnings. Further warnings will be suppressed. Please check that you have selected only date fields and try again");
                            }
                            else if (invalidDateFormatWarningsCount <= MAX_INVALID_DATE_FORMAT_WARNINGS_COUNT)
                            {
                                LOGGER.Warn("Could not convert line " + lineId + " column " + inputHeader[headerDateFieldLoc[loc]] + " to date");
                            }
                        }
                    }
                }
            }
            return inputLine;
        }

        public override string GetAuthor()
        {
            return "LeFrenchToast";
        }

        public override string GetDescription()
        {
            return "Remove dates outside of Windows epoch date range from date fields only";
        }

        public override string GetFinishedStatus()
        {
            return "Total dates removed: " + (totalDatesRemovedBeforeEpoch+totalDatesRemovedAfterMaxDate+totalDatesRemovedInvalidFormat)+
                ", before epoch: "+totalDatesRemovedBeforeEpoch+", after max date:"+totalDatesRemovedAfterMaxDate+", invalid format: "+totalDatesRemovedInvalidFormat;

        }
    }
}
