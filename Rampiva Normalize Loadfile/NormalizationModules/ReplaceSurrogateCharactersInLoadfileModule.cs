﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rampiva.LoadfileNormalizer.NormalizerModules
{

    class ReplaceSurrogateCharactersLoadfileNormalizer : NormalizationModule
    {
        private static readonly log4net.ILog LOGGER = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private List<string> loadfileHeader;
        private long totalSurrogateCharacters;

        public override List<string> NormalizeHeader(List<string> inputHeader)
        {
            LOGGER.Info("Initializing normalizer module "+this.GetType().ToString());

            // Keep track of the header, in case we need it later
            loadfileHeader = inputHeader;
            return inputHeader;
        }

        public override List<string> NormalizeLine(List<string> inputLine, long lineId)
        {
            List<string> normalizedLine = new List<string>();
            foreach (var lineElement in inputLine)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var c in lineElement)
                {
                    var isSurrogate = Char.IsSurrogate(c);
                    var isLowSurrogate = Char.IsLowSurrogate(c);
                    if ((!char.IsControl(c) && !isSurrogate) || c == '\t')
                    {
                        sb.Append(c);
                    }
                    else
                    {
                        totalSurrogateCharacters++;

                        // Only replace low surrogates with space, to avoid replacing 
                        // a surrogate pair (low+high) with 2 whitespaces
                        if (!isLowSurrogate)
                        {
                            sb.Append(' ');
                        }
                    }
                }
                normalizedLine.Add(sb.ToString());
            }
            return normalizedLine;
        }

        public override string GetAuthor()
        {
            return "Rampiva";
        }

        public override string GetDescription()
        {
            return "Replace control and surrogate characters from loadfile.dat with whitespace";
        }

        public override string GetFinishedStatus()
        {
            return "Total characters replaced in loadfile: " + totalSurrogateCharacters;
        }
    }
}
