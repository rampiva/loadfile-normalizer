﻿namespace Rampiva.LoadfileNormalizer.NormalizationModules
{
    partial class RemoveFalseDatesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RemoveFalseDatesForm));
            this.labelSelectDates = new System.Windows.Forms.Label();
            this.checkedListBoxDateFieldsHeaders = new System.Windows.Forms.CheckedListBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.checkBoxRemoveContentNotInDateFormat = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxAllowedDateFormats = new System.Windows.Forms.TextBox();
            this.linkLabelDateFormats = new System.Windows.Forms.LinkLabel();
            this.labelSee = new System.Windows.Forms.Label();
            this.buttonReset = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelSelectDates
            // 
            this.labelSelectDates.AutoSize = true;
            this.labelSelectDates.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.labelSelectDates.Location = new System.Drawing.Point(9, 189);
            this.labelSelectDates.Name = "labelSelectDates";
            this.labelSelectDates.Size = new System.Drawing.Size(116, 13);
            this.labelSelectDates.TabIndex = 0;
            this.labelSelectDates.Text = "Date fields to normalize";
            // 
            // checkedListBoxDateFieldsHeaders
            // 
            this.checkedListBoxDateFieldsHeaders.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBoxDateFieldsHeaders.CheckOnClick = true;
            this.checkedListBoxDateFieldsHeaders.FormattingEnabled = true;
            this.checkedListBoxDateFieldsHeaders.Location = new System.Drawing.Point(12, 205);
            this.checkedListBoxDateFieldsHeaders.Name = "checkedListBoxDateFieldsHeaders";
            this.checkedListBoxDateFieldsHeaders.Size = new System.Drawing.Size(610, 499);
            this.checkedListBoxDateFieldsHeaders.TabIndex = 1;
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOK.Location = new System.Drawing.Point(547, 709);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 2;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // checkBoxRemoveContentNotInDateFormat
            // 
            this.checkBoxRemoveContentNotInDateFormat.AutoSize = true;
            this.checkBoxRemoveContentNotInDateFormat.Location = new System.Drawing.Point(12, 152);
            this.checkBoxRemoveContentNotInDateFormat.Name = "checkBoxRemoveContentNotInDateFormat";
            this.checkBoxRemoveContentNotInDateFormat.Size = new System.Drawing.Size(229, 17);
            this.checkBoxRemoveContentNotInDateFormat.TabIndex = 3;
            this.checkBoxRemoveContentNotInDateFormat.Text = "Remove content not in allowed date format";
            this.checkBoxRemoveContentNotInDateFormat.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Allowed date formats";
            // 
            // textBoxAllowedDateFormats
            // 
            this.textBoxAllowedDateFormats.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxAllowedDateFormats.Location = new System.Drawing.Point(12, 25);
            this.textBoxAllowedDateFormats.Multiline = true;
            this.textBoxAllowedDateFormats.Name = "textBoxAllowedDateFormats";
            this.textBoxAllowedDateFormats.Size = new System.Drawing.Size(610, 84);
            this.textBoxAllowedDateFormats.TabIndex = 5;
            // 
            // linkLabelDateFormats
            // 
            this.linkLabelDateFormats.AutoSize = true;
            this.linkLabelDateFormats.Location = new System.Drawing.Point(40, 112);
            this.linkLabelDateFormats.Name = "linkLabelDateFormats";
            this.linkLabelDateFormats.Size = new System.Drawing.Size(473, 13);
            this.linkLabelDateFormats.TabIndex = 6;
            this.linkLabelDateFormats.TabStop = true;
            this.linkLabelDateFormats.Text = "https://docs.microsoft.com/en-us/dotnet/standard/base-types/custom-date-and-time-" +
    "format-strings";
            this.linkLabelDateFormats.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelDateFormats_LinkClicked);
            // 
            // labelSee
            // 
            this.labelSee.AutoSize = true;
            this.labelSee.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.labelSee.Location = new System.Drawing.Point(12, 112);
            this.labelSee.Name = "labelSee";
            this.labelSee.Size = new System.Drawing.Size(26, 13);
            this.labelSee.TabIndex = 7;
            this.labelSee.Text = "See";
            // 
            // buttonReset
            // 
            this.buttonReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReset.Location = new System.Drawing.Point(547, 112);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(75, 23);
            this.buttonReset.TabIndex = 8;
            this.buttonReset.Text = "Reset";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // RemoveFalseDatesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 744);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.labelSee);
            this.Controls.Add(this.linkLabelDateFormats);
            this.Controls.Add(this.textBoxAllowedDateFormats);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkBoxRemoveContentNotInDateFormat);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.checkedListBoxDateFieldsHeaders);
            this.Controls.Add(this.labelSelectDates);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1920, 1080);
            this.MinimumSize = new System.Drawing.Size(650, 400);
            this.Name = "RemoveFalseDatesForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Normlize date fields";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelSelectDates;
        public System.Windows.Forms.CheckedListBox checkedListBoxDateFieldsHeaders;
        public System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.CheckBox checkBoxRemoveContentNotInDateFormat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxAllowedDateFormats;
        private System.Windows.Forms.LinkLabel linkLabelDateFormats;
        private System.Windows.Forms.Label labelSee;
        public System.Windows.Forms.Button buttonReset;
    }
}