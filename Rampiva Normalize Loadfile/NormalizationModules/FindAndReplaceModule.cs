﻿using Rampiva.LoadfileNormalizer.NormalizationModules;
using System;
using System.Collections.Generic;
using System.Linq;
using Rampiva.LoadfileNormalizer.Utils;
using System.Globalization;
using ZetaLongPaths;

namespace Rampiva.LoadfileNormalizer.NormalizerModules
{
    class FindAndReplaceModule : NormalizationModule
    {
        private static readonly log4net.ILog LOGGER = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private List<int> headerFieldLoc;
        private long totalLinesChanged;
        private long totalRecordsChanged;

        private long renamedNativeFiles;
        private long renamedTextFiles;

        private int nativeFieldLoc;
        private int textFieldLoc;

        private FindAndReplaceForm myForm;
        private List<string> inputHeader;
        private string searchForText;
        private string replaceWithText;

        bool renameNativeFiles;
        bool renameTextFiles;
        ZlpDirectoryInfo loadFileDi;

        public override void setLoadFileFolder(ZlpDirectoryInfo di)
        {
            loadFileDi = di;
        }

        public override List<string> NormalizeHeader(List<string> inputHeader)
        {
            headerFieldLoc = new List<int>();
            totalLinesChanged = 0;
            totalRecordsChanged = 0;

            renamedNativeFiles = 0;
            renamedTextFiles = 0;

            nativeFieldLoc = -1;
            textFieldLoc = -1;

            this.inputHeader = inputHeader;
            using (myForm = new FindAndReplaceForm())
            {
                for (int a = 0; a < inputHeader.Count(); a++)
                {
                    myForm.checkedListBoxDateFieldsHeaders.Items.Add(inputHeader[a]);
                    myForm.checkedListBoxDateFieldsHeaders.SetItemChecked(myForm.checkedListBoxDateFieldsHeaders.Items.Count - 1, OptionsPersistance.getInstance().getBooleanOption("ReplaceInFields", inputHeader[a], false));

                }
                myForm.ShowDialog();

                if (myForm.GetAccept())
                {
                    if (myForm.checkedListBoxDateFieldsHeaders.CheckedItems.Count != 0)
                    {
                        for (int id = 0; id < myForm.checkedListBoxDateFieldsHeaders.Items.Count; id++)
                        {
                            bool isItemChecked = myForm.checkedListBoxDateFieldsHeaders.GetItemChecked(id);
                            if (isItemChecked)
                            {
                                headerFieldLoc.Add(id);
                            }

                            if (inputHeader[id] == "ITEMPATH")
                            {
                                nativeFieldLoc = id;
                            }
                            else if (inputHeader[id] == "TEXTPATH")
                            {
                                textFieldLoc = id;
                            }

                            OptionsPersistance.getInstance().setBooleanOption("FindAndReplace", inputHeader[id], isItemChecked);
                        }
                    }
                    else
                    {
                        //Stop doing work
                    }
                }

                searchForText = myForm.textBoxOriginalText.Text;
                replaceWithText = myForm.textBoxReplaceWith.Text;
                renameNativeFiles = myForm.checkBoxReplaceInNativeFilename.Checked;
                renameTextFiles = myForm.checkBoxReplaceInTextFilename.Checked;

            }
            return inputHeader;
        }

        public override List<string> NormalizeLine(List<string> inputLine, long lineId)
        {

            try
            {
                if (renameNativeFiles)
                {
                    var sourceFileName = inputLine[nativeFieldLoc];
                    var sourceFullFileName = loadFileDi.FullName + "\\" + sourceFileName;
                    ZlpFileInfo fi = new ZlpFileInfo(sourceFullFileName);
                    var newName = fi.Name.Replace(searchForText, fi.Directory.FullName + "\\" + replaceWithText);
                    if (newName != fi.Name)
                    {
                        fi.MoveTo(newName);
                        renamedNativeFiles++;
                    }
                }
            }
            catch (Exception e)
            {
                LOGGER.Error("Cannot rename file, ", e);
            }

            try
            {
                if (renameTextFiles)
                {
                    var sourceFileName = inputLine[textFieldLoc];
                    var sourceFullFileName = loadFileDi.FullName + "\\" + sourceFileName;
                    ZlpFileInfo fi = new ZlpFileInfo(sourceFullFileName);
                    var newName = fi.Name.Replace(searchForText, fi.Directory.FullName + "\\" + replaceWithText);
                    if (newName != fi.Name)
                    {
                        fi.MoveTo(newName);
                        renamedTextFiles++;
                    }
                }
            }
            catch (Exception e)
            {
                LOGGER.Error("Cannot rename file, ", e);
            }

            bool lineChanged = false;
            for (int loc = 0; loc < headerFieldLoc.Count(); loc++)
            {
                if (inputLine[headerFieldLoc[loc]].Length > 0)
                {
                    var sourceText = inputLine[headerFieldLoc[loc]];
                    var newText = sourceText.Replace(searchForText, replaceWithText);
                    if (sourceText != newText)
                    {
                        inputLine[headerFieldLoc[loc]] = newText;
                        totalRecordsChanged++;
                        lineChanged = true;
                    }
                }
            }

            if (lineChanged)
            {
                totalLinesChanged++;
            }

            return inputLine;
        }

        public override string GetAuthor()
        {
            return "Rampiva";
        }

        public override string GetDescription()
        {
            return "Find and replace";
        }

        public override string GetFinishedStatus()
        {

            return "Changed " + totalRecordsChanged + " field values" +
                                (renameNativeFiles ? (", renamed " + renamedNativeFiles + " native files") : "") +
                                (renameTextFiles ? (", renamed " + renamedTextFiles + " text files") : "");
        }
    }
}
