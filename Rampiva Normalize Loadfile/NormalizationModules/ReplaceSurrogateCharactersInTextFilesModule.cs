﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using ZetaLongPaths;
using Rampiva.LoadfileNormalizer.NormalizationModules;

namespace Rampiva.LoadfileNormalizer.NormalizerModules
{

    class ReplaceSurrogateCharactersInTextFilesModule : NormalizationModule
    {
        private static readonly log4net.ILog LOGGER = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private List<string> loadfileHeader;
        private int textPosition;
        private long totalSurrogateCharacters;
        private long filesWithSurrogateCharacters;
        private ZlpDirectoryInfo loadfileDi;
        private string textPathFieldName;


        public override List<string> NormalizeHeader(List<string> inputHeader)
        {


            LOGGER.Info("Initializing normalizer module "+this.GetType().ToString());

            using (var myForm = new ReplaceSurrogateCharactersInTextFilesForm(inputHeader))
            {
                myForm.ShowDialog();

                if (myForm.GetAccept())
                {
                    textPathFieldName = myForm.GetTextPathField();
                }
                else
                {
                    textPathFieldName = null;
                }
            }

            textPosition = -1;
            totalSurrogateCharacters = 0;
            filesWithSurrogateCharacters = 0;

            // Keep track of the header, in case we need it later
            loadfileHeader = inputHeader;
            
            for (int i = 0; i < inputHeader.Count; i++)
            {
                if (textPathFieldName!=null && inputHeader[i]== textPathFieldName)
                {
                    LOGGER.Info("Text path field " + i + ": " + inputHeader[i]);
                    textPosition = i;
                    break;
                }
            }
            return inputHeader;
        }

        public override List<string> NormalizeLine(List<string> inputLine, long lineId)
        {
            List<string> normalizedLine = new List<string>();            
            if (textPosition >= 0)
            {
                var originalTextFile = inputLine[textPosition];

                // TODO - change to stream read so that the entire file is not loaded into memory
                var textContent = File.ReadAllText(loadfileDi.FullName+"\\"+originalTextFile);
                StringBuilder sb = new StringBuilder();

                long surrogateCharactersInFile = 0;
                foreach (var c in textContent)
                {
                    var isSurrogate = Char.IsSurrogate(c);
                    var isLowSurrogate = Char.IsLowSurrogate(c);
                    if ((!char.IsControl(c) && !isSurrogate) || c == '\t' || c == '\r' || c == '\n')
                    {
                        sb.Append(c);
                    }
                    else
                    {
                        totalSurrogateCharacters++;
                        surrogateCharactersInFile++;

                        // Only replace low surrogates with space, to avoid replacing 
                        // a surrogate pair (low+high) with 2 whitespaces
                        if (!isLowSurrogate)
                        {
                            sb.Append(' ');
                        }
                    }
                }
                

                if (surrogateCharactersInFile > 0)
                {
                    normalizedLine.AddRange(inputLine);
                    filesWithSurrogateCharacters++;

                    FileInfo originalFi = new FileInfo(originalTextFile);
                    var textFileName = originalFi.Name;
                    var textFileFolder = originalTextFile.Substring(0, originalTextFile.Length-originalFi.Name.Length);


                    textFileName = textFileName.Substring(0, textFileName.Length - (originalFi.Extension.Length));
                    textFileName = textFileName + ".normalized.txt";

                    File.WriteAllText(loadfileDi.FullName + "\\" + textFileFolder + textFileName, sb.ToString());
                    normalizedLine[textPosition] = textFileFolder + textFileName;

                    return normalizedLine;
                }
            }

            return inputLine;
        }

        public override void setLoadFileFolder(ZlpDirectoryInfo di)
        {
            loadfileDi = di;
        }


        public override string GetAuthor()
        {
            return "Rampiva";
        }

        public override string GetDescription()
        {
            return "Replace control and surrogate characters from text file with whitespace";
        }

        public override string GetFinishedStatus()
        {
            return "Total characters replaced in text files: " + totalSurrogateCharacters+", "+
                "Total text files normalized: " + filesWithSurrogateCharacters;                
        }
    }
}
