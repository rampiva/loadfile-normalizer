﻿using Rampiva.LoadfileNormalizer.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rampiva.LoadfileNormalizer
{
    static class Program
    {
        private static readonly log4net.ILog LOGGER = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            string version = System.Reflection.Assembly.GetExecutingAssembly()
                               .GetName()
                               .Version
                               .ToString();
            string softwareName = System.Reflection.Assembly.GetExecutingAssembly()
                                           .GetName().Name;
            LOGGER.Info(softwareName + " Version " + version+" starting");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormMain());
            LOGGER.Info(softwareName + " Version " + version + " closing");
        }
    }
}
