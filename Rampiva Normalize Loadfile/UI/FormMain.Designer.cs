﻿namespace Rampiva.LoadfileNormalizer.UI
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.imageListIcons = new System.Windows.Forms.ImageList(this.components);
            this.splitContainerMain = new System.Windows.Forms.SplitContainer();
            this.label2 = new System.Windows.Forms.Label();
            this.checkedListBoxOptions = new System.Windows.Forms.CheckedListBox();
            this.buttonSourceLocationBrowse = new System.Windows.Forms.Button();
            this.labelSourceLocation = new System.Windows.Forms.Label();
            this.textBoxSourceLocation = new System.Windows.Forms.TextBox();
            this.buttonStop = new System.Windows.Forms.Button();
            this.labelProgressSanitizeLoadFilePercentage = new System.Windows.Forms.Label();
            this.labelProgressSanitizeLoadfileCount = new System.Windows.Forms.Label();
            this.progressBarSanitizeLoadfile = new System.Windows.Forms.ProgressBar();
            this.labelSanitizeLoadfile = new System.Windows.Forms.Label();
            this.buttonNormalize = new System.Windows.Forms.Button();
            this.listBoxLog = new System.Windows.Forms.ListBox();
            this.labelLog = new System.Windows.Forms.Label();
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showLogLocationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.minimizeLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerMain)).BeginInit();
            this.splitContainerMain.Panel1.SuspendLayout();
            this.splitContainerMain.Panel2.SuspendLayout();
            this.splitContainerMain.SuspendLayout();
            this.menuStripMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // imageListIcons
            // 
            this.imageListIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListIcons.ImageStream")));
            this.imageListIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListIcons.Images.SetKeyName(0, "Forward.png");
            this.imageListIcons.Images.SetKeyName(1, "Downloads.png");
            this.imageListIcons.Images.SetKeyName(2, "Close Window_96px.png");
            this.imageListIcons.Images.SetKeyName(3, "Housekeeping_96px.png");
            this.imageListIcons.Images.SetKeyName(4, "Witch_96px.png");
            // 
            // splitContainerMain
            // 
            this.splitContainerMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerMain.Location = new System.Drawing.Point(0, 118);
            this.splitContainerMain.Name = "splitContainerMain";
            this.splitContainerMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerMain.Panel1
            // 
            this.splitContainerMain.Panel1.Controls.Add(this.label2);
            this.splitContainerMain.Panel1.Controls.Add(this.checkedListBoxOptions);
            this.splitContainerMain.Panel1.Controls.Add(this.buttonSourceLocationBrowse);
            this.splitContainerMain.Panel1.Controls.Add(this.labelSourceLocation);
            this.splitContainerMain.Panel1.Controls.Add(this.textBoxSourceLocation);
            this.splitContainerMain.Panel1.Controls.Add(this.buttonStop);
            this.splitContainerMain.Panel1.Controls.Add(this.labelProgressSanitizeLoadFilePercentage);
            this.splitContainerMain.Panel1.Controls.Add(this.labelProgressSanitizeLoadfileCount);
            this.splitContainerMain.Panel1.Controls.Add(this.progressBarSanitizeLoadfile);
            this.splitContainerMain.Panel1.Controls.Add(this.labelSanitizeLoadfile);
            this.splitContainerMain.Panel1.Controls.Add(this.buttonNormalize);
            // 
            // splitContainerMain.Panel2
            // 
            this.splitContainerMain.Panel2.Controls.Add(this.listBoxLog);
            this.splitContainerMain.Panel2.Controls.Add(this.labelLog);
            this.splitContainerMain.Size = new System.Drawing.Size(869, 565);
            this.splitContainerMain.SplitterDistance = 355;
            this.splitContainerMain.TabIndex = 100;
            this.splitContainerMain.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 76;
            this.label2.Text = "Options";
            // 
            // checkedListBoxOptions
            // 
            this.checkedListBoxOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBoxOptions.CheckOnClick = true;
            this.checkedListBoxOptions.FormattingEnabled = true;
            this.checkedListBoxOptions.Location = new System.Drawing.Point(12, 68);
            this.checkedListBoxOptions.Name = "checkedListBoxOptions";
            this.checkedListBoxOptions.Size = new System.Drawing.Size(845, 154);
            this.checkedListBoxOptions.TabIndex = 74;
            // 
            // buttonSourceLocationBrowse
            // 
            this.buttonSourceLocationBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSourceLocationBrowse.Location = new System.Drawing.Point(782, 13);
            this.buttonSourceLocationBrowse.Name = "buttonSourceLocationBrowse";
            this.buttonSourceLocationBrowse.Size = new System.Drawing.Size(75, 23);
            this.buttonSourceLocationBrowse.TabIndex = 72;
            this.buttonSourceLocationBrowse.Text = "Browse";
            this.buttonSourceLocationBrowse.UseVisualStyleBackColor = true;
            this.buttonSourceLocationBrowse.Click += new System.EventHandler(this.buttonSourceLocationBrowse_Click);
            // 
            // labelSourceLocation
            // 
            this.labelSourceLocation.AutoSize = true;
            this.labelSourceLocation.Location = new System.Drawing.Point(7, 0);
            this.labelSourceLocation.Name = "labelSourceLocation";
            this.labelSourceLocation.Size = new System.Drawing.Size(77, 13);
            this.labelSourceLocation.TabIndex = 73;
            this.labelSourceLocation.Text = "Source loadfile";
            // 
            // textBoxSourceLocation
            // 
            this.textBoxSourceLocation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSourceLocation.Location = new System.Drawing.Point(12, 16);
            this.textBoxSourceLocation.Name = "textBoxSourceLocation";
            this.textBoxSourceLocation.Size = new System.Drawing.Size(764, 20);
            this.textBoxSourceLocation.TabIndex = 71;
            this.textBoxSourceLocation.TextChanged += new System.EventHandler(this.textBoxSourceLocation_TextChanged);
            // 
            // buttonStop
            // 
            this.buttonStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonStop.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonStop.ImageIndex = 2;
            this.buttonStop.ImageList = this.imageListIcons;
            this.buttonStop.Location = new System.Drawing.Point(769, 310);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(88, 42);
            this.buttonStop.TabIndex = 70;
            this.buttonStop.Text = "   Stop";
            this.buttonStop.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.button4ProgressStop_Click);
            // 
            // labelProgressSanitizeLoadFilePercentage
            // 
            this.labelProgressSanitizeLoadFilePercentage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelProgressSanitizeLoadFilePercentage.AutoSize = true;
            this.labelProgressSanitizeLoadFilePercentage.Location = new System.Drawing.Point(824, 285);
            this.labelProgressSanitizeLoadFilePercentage.Name = "labelProgressSanitizeLoadFilePercentage";
            this.labelProgressSanitizeLoadFilePercentage.Size = new System.Drawing.Size(24, 13);
            this.labelProgressSanitizeLoadFilePercentage.TabIndex = 69;
            this.labelProgressSanitizeLoadFilePercentage.Text = "n/a";
            this.labelProgressSanitizeLoadFilePercentage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelProgressSanitizeLoadfileCount
            // 
            this.labelProgressSanitizeLoadfileCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelProgressSanitizeLoadfileCount.AutoSize = true;
            this.labelProgressSanitizeLoadfileCount.Location = new System.Drawing.Point(12, 285);
            this.labelProgressSanitizeLoadfileCount.Name = "labelProgressSanitizeLoadfileCount";
            this.labelProgressSanitizeLoadfileCount.Size = new System.Drawing.Size(24, 13);
            this.labelProgressSanitizeLoadfileCount.TabIndex = 68;
            this.labelProgressSanitizeLoadfileCount.Text = "n/a";
            // 
            // progressBarSanitizeLoadfile
            // 
            this.progressBarSanitizeLoadfile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarSanitizeLoadfile.Location = new System.Drawing.Point(12, 259);
            this.progressBarSanitizeLoadfile.Name = "progressBarSanitizeLoadfile";
            this.progressBarSanitizeLoadfile.Size = new System.Drawing.Size(845, 23);
            this.progressBarSanitizeLoadfile.Step = 1;
            this.progressBarSanitizeLoadfile.TabIndex = 67;
            // 
            // labelSanitizeLoadfile
            // 
            this.labelSanitizeLoadfile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelSanitizeLoadfile.AutoSize = true;
            this.labelSanitizeLoadfile.Location = new System.Drawing.Point(9, 243);
            this.labelSanitizeLoadfile.Name = "labelSanitizeLoadfile";
            this.labelSanitizeLoadfile.Size = new System.Drawing.Size(111, 13);
            this.labelSanitizeLoadfile.TabIndex = 66;
            this.labelSanitizeLoadfile.Text = "Progress (loadfile size)";
            // 
            // buttonNormalize
            // 
            this.buttonNormalize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNormalize.Enabled = false;
            this.buttonNormalize.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonNormalize.ImageKey = "Forward.png";
            this.buttonNormalize.ImageList = this.imageListIcons;
            this.buttonNormalize.Location = new System.Drawing.Point(675, 310);
            this.buttonNormalize.Name = "buttonNormalize";
            this.buttonNormalize.Size = new System.Drawing.Size(88, 42);
            this.buttonNormalize.TabIndex = 65;
            this.buttonNormalize.Text = "   Normalize";
            this.buttonNormalize.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonNormalize.UseVisualStyleBackColor = true;
            this.buttonNormalize.Click += new System.EventHandler(this.buttonOptionsNext_Click);
            // 
            // listBoxLog
            // 
            this.listBoxLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxLog.FormattingEnabled = true;
            this.listBoxLog.Location = new System.Drawing.Point(10, 16);
            this.listBoxLog.Name = "listBoxLog";
            this.listBoxLog.Size = new System.Drawing.Size(847, 160);
            this.listBoxLog.TabIndex = 101;
            this.listBoxLog.TabStop = false;
            // 
            // labelLog
            // 
            this.labelLog.AutoSize = true;
            this.labelLog.Location = new System.Drawing.Point(7, 0);
            this.labelLog.Name = "labelLog";
            this.labelLog.Size = new System.Drawing.Size(25, 13);
            this.labelLog.TabIndex = 2;
            this.labelLog.Text = "Log";
            // 
            // menuStripMain
            // 
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStripMain.Location = new System.Drawing.Point(0, 0);
            this.menuStripMain.Name = "menuStripMain";
            this.menuStripMain.Size = new System.Drawing.Size(869, 24);
            this.menuStripMain.TabIndex = 13;
            this.menuStripMain.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showLogToolStripMenuItem,
            this.showLogLocationToolStripMenuItem,
            this.minimizeLogToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // showLogToolStripMenuItem
            // 
            this.showLogToolStripMenuItem.Enabled = false;
            this.showLogToolStripMenuItem.Name = "showLogToolStripMenuItem";
            this.showLogToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.showLogToolStripMenuItem.Text = "Show Log";
            this.showLogToolStripMenuItem.Click += new System.EventHandler(this.showLogToolStripMenuItem_Click);
            // 
            // showLogLocationToolStripMenuItem
            // 
            this.showLogLocationToolStripMenuItem.Name = "showLogLocationToolStripMenuItem";
            this.showLogLocationToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.showLogLocationToolStripMenuItem.Text = "Show Log Location";
            this.showLogLocationToolStripMenuItem.Click += new System.EventHandler(this.showLogLocationToolStripMenuItem_Click);
            // 
            // minimizeLogToolStripMenuItem
            // 
            this.minimizeLogToolStripMenuItem.Name = "minimizeLogToolStripMenuItem";
            this.minimizeLogToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.minimizeLogToolStripMenuItem.Text = "Minimize Log";
            this.minimizeLogToolStripMenuItem.Click += new System.EventHandler(this.minimizeLogToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxLogo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.pictureBoxLogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxLogo.BackgroundImage")));
            this.pictureBoxLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBoxLogo.Location = new System.Drawing.Point(0, 27);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(879, 85);
            this.pictureBoxLogo.TabIndex = 14;
            this.pictureBoxLogo.TabStop = false;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(869, 682);
            this.Controls.Add(this.pictureBoxLogo);
            this.Controls.Add(this.splitContainerMain);
            this.Controls.Add(this.menuStripMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStripMain;
            this.Name = "FormMain";
            this.Text = "Rampiva Loadfile Normalizer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.splitContainerMain.Panel1.ResumeLayout(false);
            this.splitContainerMain.Panel1.PerformLayout();
            this.splitContainerMain.Panel2.ResumeLayout(false);
            this.splitContainerMain.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerMain)).EndInit();
            this.splitContainerMain.ResumeLayout(false);
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.SplitContainer splitContainerMain;
        private System.Windows.Forms.Label labelLog;
        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showLogLocationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem minimizeLogToolStripMenuItem;
        private System.Windows.Forms.ListBox listBoxLog;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ImageList imageListIcons;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckedListBox checkedListBoxOptions;
        private System.Windows.Forms.Button buttonSourceLocationBrowse;
        private System.Windows.Forms.Label labelSourceLocation;
        private System.Windows.Forms.TextBox textBoxSourceLocation;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Label labelProgressSanitizeLoadFilePercentage;
        private System.Windows.Forms.Label labelProgressSanitizeLoadfileCount;
        private System.Windows.Forms.ProgressBar progressBarSanitizeLoadfile;
        private System.Windows.Forms.Label labelSanitizeLoadfile;
        private System.Windows.Forms.Button buttonNormalize;
    }
}

