﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text;
using ZetaLongPaths;
using System.Linq;
using System.Reflection;
using Rampiva.LoadfileNormalizer.NormalizerModules;
using Rampiva.LoadfileNormalizer.Utils;

namespace Rampiva.LoadfileNormalizer.UI
{

    public partial class FormMain : Form
    {
        private static readonly log4net.ILog LOGGER = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public bool UILoggingEnabled = true;
        private bool normalizationInProgress = false;




        public FormMain()
        {
            InitializeComponent();
            setNormalizationInProgress(false);

            //Detecting all normalizers

            var normalizerInterfaceType = typeof(NormalizationModule);
            var normalizerTypes = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => normalizerInterfaceType.IsAssignableFrom(p) && p.IsClass && !p.IsAbstract);
 
            foreach (var normalizerType in normalizerTypes)
            {
                var normalizer = (NormalizationModule)Activator.CreateInstance(normalizerType);
                checkedListBoxOptions.Items.Add(normalizer);
                checkedListBoxOptions.SetItemChecked(checkedListBoxOptions.Items.Count - 1, OptionsPersistance.getInstance().getBooleanOption("Modules",normalizer.GetType().ToString(), false));
            }
        }


        private void showLog(bool status)
        {
            UILoggingEnabled = status;
            splitContainerMain.Panel2Collapsed = !status;
            showLogToolStripMenuItem.Enabled = !status;
            minimizeLogToolStripMenuItem.Enabled = status;
        }

        private void buttonHideLog_Click(object sender, EventArgs e)
        {
            showLog(false);
        }

        private void showLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showLog(true);
        }

        private void showLogLocationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string logsLocation = Environment.GetEnvironmentVariable("LocalAppData") + "\\Rampiva\\LoadfileNormalizer";
            Process.Start(logsLocation);
        }

        private void minimizeLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showLog(false);
        }


        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string version = System.Reflection.Assembly.GetExecutingAssembly()
                                           .GetName()
                                           .Version
                                           .ToString();
            string softwareName = System.Reflection.Assembly.GetExecutingAssembly()
                                           .GetName().Name;


            string licence = "";
            var attributes = Assembly.GetEntryAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
            if (attributes.Length > 0)
            {
                licence = (attributes[0] as AssemblyCopyrightAttribute).Copyright;
            }

            string sourceCodeLocation = "https://bitbucket.org/rampiva/loadfile-normalizer";

            MessageBox.Show(softwareName + "\nVersion " + version + "\n\n"+ licence+"\n\n"+ sourceCodeLocation, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        Task normalizeLoadFileTask;
        Worker worker;

        private void startNormalization()
        {

            setNormalizationInProgress(true);
            LOGGER.Info("------------------------");
            LOGGER.Info("Starting normalization of " + textBoxSourceLocation.Text);

            Task.Factory.StartNew(() => updateProgress());

            ZlpFileInfo sourceLoadfileFi = new ZlpFileInfo(textBoxSourceLocation.Text);
            ZlpFileInfo normalizedLoadfileFi = new ZlpFileInfo(textBoxSourceLocation.Text.Replace(sourceLoadfileFi.Extension, ".normalized.dat"));

            List<NormalizationModule> selectedNormalizers = new List<NormalizationModule>();
            for (int i = 0; i < checkedListBoxOptions.Items.Count; i++)
            {
                NormalizationModule normalizer = (NormalizationModule)checkedListBoxOptions.Items[i];
                bool isModuleChecked = checkedListBoxOptions.GetItemChecked(i);
                if (isModuleChecked)
                {
                    selectedNormalizers.Add(normalizer);
                }
                OptionsPersistance.getInstance().setBooleanOption("Modules", normalizer.GetType().ToString(), isModuleChecked);
            }
            worker = new Worker(selectedNormalizers, sourceLoadfileFi.FullName, normalizedLoadfileFi.FullName);
            normalizeLoadFileTask = Task.Factory.StartNew(() => worker.DoWork());
        }


        public void updateProgress(ProgressBar progressBar, Label countLabel, Label percentageLabel, long completedTasks, long totalTasks)
        {
            countLabel.Invoke((MethodInvoker)delegate
            {
                countLabel.Text = "" + completedTasks + " / " + totalTasks;
            });

            string progressPercentage = "n/a";
            if (completedTasks > 0)
            {
                progressPercentage = "" + ((int)(100.0 * completedTasks / totalTasks)) + "%";
            }

            percentageLabel.Invoke((MethodInvoker)delegate
            {
                percentageLabel.Text = progressPercentage;
            });


            double normalizedTotalTasks = totalTasks;
            double normalizedCompletedTasks = completedTasks;
            while (normalizedTotalTasks > int.MaxValue)
            {
                normalizedTotalTasks = normalizedTotalTasks / 10.0;
                normalizedCompletedTasks = completedTasks / 10.0;
            }

            progressBar.Invoke((MethodInvoker)delegate
            {
                progressBar.Maximum = (int)normalizedTotalTasks;
                progressBar.Value = (int)normalizedCompletedTasks;
            });

        }

        public void updateProgress()
        {
            while (true)
            {
                try
                {
                    if (worker != null)
                    {
                        updateProgress(progressBarSanitizeLoadfile, labelProgressSanitizeLoadfileCount, labelProgressSanitizeLoadFilePercentage, (int)Math.Ceiling(worker.LoadfileProgressSize / 1024.0), (int)Math.Ceiling(worker.LoadfileTotalSize / 1024.0));
                    }
                }
                catch (ThreadAbortException)
                {
                    // If thread aborting, abort display
                    return;
                }
                catch (Exception e)
                {
                    LOGGER.Warn("Cannot update progress. ", e);
                }

                try
                {
                    if (normalizationInProgress)
                    {
                        if (normalizeLoadFileTask != null && normalizeLoadFileTask.IsCompleted)
                        {
                            setNormalizationInProgress(false);
                            this.Invoke((MethodInvoker)delegate
                            {
                                buttonStop.Enabled = false;
                            });

                            if (!worker.ErrorsEncountered)
                            {
                                LOGGER.Info("Normalization complete");
                                MessageBox.Show("Normalization complete", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                            else
                            {
                                MessageBox.Show("Normalization failed, see log for details", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }

                        }
                    }
                }
                catch (Exception e)
                {
                    LOGGER.Warn("Cannot update progress. ", e);
                }
                System.Threading.Thread.Sleep(100);
            }
        }

        private bool stopNormalization()
        {
            if (normalizeLoadFileTask != null && !normalizeLoadFileTask.IsCompleted)
            {
                var result = MessageBox.Show("Normalization in progress. Are you sure you want to abort?", this.Text, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    worker.Abort();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }

        }

        private void button4ProgressStop_Click(object sender, EventArgs e)
        {
            stopNormalization();

        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!stopNormalization())
            {
                e.Cancel = true;
            }
        }


        private void buttonSourceLocationBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Select loadfile to normalize";
            openFileDialog.Filter = "Concordance Loadfile (*.dat)|*.dat|All files (*.*)|*.*";
            openFileDialog.FilterIndex = 1;
            openFileDialog.RestoreDirectory = true;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                textBoxSourceLocation.Text = openFileDialog.FileName;
            }
            checkNextPossible();
        }


        void checkNextPossible()
        {
            buttonNormalize.Enabled = checkLoadfileValidity() && !normalizationInProgress;
        }

        bool checkLoadfileValidity()
        {
            if (textBoxSourceLocation.Text.Length == 0)
            {
                return false;
            }

            if (!File.Exists(textBoxSourceLocation.Text))
            {
                return false;
            }
            return true;
        }

        private void setNormalizationInProgress(bool running)
        {
            if (this.Visible)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    setComponentsEnabled(running);
                });
            }
            else
            {
                setComponentsEnabled(running);
            }

        }

        private void setComponentsEnabled(bool running)
        {
            normalizationInProgress = running;
            textBoxSourceLocation.Enabled = !running;
            buttonSourceLocationBrowse.Enabled = !running;
            checkedListBoxOptions.Enabled = !running;
            buttonNormalize.Enabled = !running && checkLoadfileValidity();
            buttonStop.Enabled = running;
        }

        private void textBoxSourceLocation_TextChanged(object sender, EventArgs e)
        {
            checkNextPossible();
        }

        private void buttonOptionsNext_Click(object sender, EventArgs e)
        {
            startNormalization();
        }
    }
}
