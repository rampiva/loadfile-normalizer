﻿using log4net.Appender;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rampiva.LoadfileNormalizer.Utils
{
    public class TextBoxAppender : AppenderSkeleton
    {

        public TextBoxAppender()
        {
            

        }

        Task refreshDisplayTask = null;
        private List<string> logLines = new List<string>();
        private const int maxLogLines = 100;
        private ListBox listBox;
        private Rampiva.LoadfileNormalizer.UI.FormMain formMain;
        bool logChanged = false;

        public ListBox AppenderTextBox
        {
            get
            {
                return listBox;
            }
            set
            {
                listBox = value;
            }
        }
        public string FormName { get; set; }
        public string TextBoxName { get; set; }

        private Control FindControlRecursive(Control root, string textBoxName)
        {
            if (root.Name == textBoxName) return root;
            foreach (Control c in root.Controls)
            {
                Control t = FindControlRecursive(c, textBoxName);
                if (t != null) return t;
            }
            return null;
        }

        private void getControl()
        {
            if (String.IsNullOrEmpty(FormName) ||
                String.IsNullOrEmpty(TextBoxName))
                return;

            Form form = Application.OpenForms[FormName];
            if (form == null)
                return;

            try
            { formMain = (Rampiva.LoadfileNormalizer.UI.FormMain)form; }
            catch { }

            listBox = (ListBox)FindControlRecursive(form, TextBoxName);

            if (listBox == null)
                return;

            form.FormClosing += (s, e) => listBox = null;
        }

        public void refreshDisplay()
        {
            while (true)
            {
                if (logChanged)
                {
                    if (listBox == null)
                    {
                        getControl();
                    }

                    try
                    {
                        if (formMain!=null && formMain.UILoggingEnabled)
                        {
                            listBox.BeginInvoke((MethodInvoker)delegate
                            {
                                listBox.Suspend();
                                listBox.Items.Clear();
                                lock (logLines)
                                {
                                    logChanged = false;
                                    foreach (var logLine in logLines)
                                    {
                                        listBox.Items.Add(logLine);
                                    }
                                }
                                listBox.SelectedIndex = listBox.Items.Count - 1;
                                listBox.ClearSelected();

                                listBox.Resume();

                            });
                        }
                    }
                    catch
                    {
                        //Silently catch exceptions related to logging to the textbox
                    }
                }
                System.Threading.Thread.Sleep(500);
            }
        }

        protected override void Append(log4net.Core.LoggingEvent loggingEvent)
        {
            if (refreshDisplayTask == null)
            {
                refreshDisplayTask =  Task.Factory.StartNew(() => refreshDisplay());
            }

            lock (logLines)
            {
                logChanged = true;
                logLines.Add(RenderLoggingEvent(loggingEvent));
                while (logLines.Count > maxLogLines)
                {
                    logLines.RemoveAt(0);
                }
            }
        }
    }
}
