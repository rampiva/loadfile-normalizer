﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace Rampiva.LoadfileNormalizer.Utils
{
    class OptionsPersistance
    {
        private static readonly log4net.ILog LOGGER = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static OptionsPersistance instance = null;
        private const string userRoot = "HKEY_CURRENT_USER";
        private const string subkey = "Software\\Rampiva\\Loadfile Normalizer";
        private const string keyName = userRoot + "\\" + subkey;

        public static OptionsPersistance getInstance()
        {
            if (instance == null)
            {
                instance = new OptionsPersistance();
            }
            return instance;
        }

        public void setMultiTextOption(string subKeyName, string optionName, string[] optionValue)
        {
            Registry.SetValue((subKeyName != null && subKeyName.Length > 0) ? keyName + "\\" + subKeyName : keyName, optionName, optionValue);
        }

        public string[] getMultiTextOption(string subKeyName, string optionName, string[] defaultValue)
        {
            try
            {
                return ((string[])Registry.GetValue((subKeyName != null && subKeyName.Length > 0) ? keyName + "\\" + subKeyName : keyName, optionName, defaultValue));
            }
            catch (Exception e)
            {
                LOGGER.Debug("Cannot get registry value " + ((subKeyName != null && subKeyName.Length > 0) ? keyName + "\\" + subKeyName : keyName) + " - " + optionName, e);
                return defaultValue;
            }
        }


        public void setTextOption(string subKeyName, string optionName, string optionValue)
        {
            Registry.SetValue((subKeyName!=null && subKeyName.Length>0)?keyName+"\\"+subKeyName:keyName, optionName, optionValue);
        }

        public string getTextOption(string subKeyName, string optionName, string defaultValue = "")
        {
            try
            {
                return (string)Registry.GetValue((subKeyName != null && subKeyName.Length > 0) ? keyName + "\\" + subKeyName : keyName, optionName, defaultValue);
            }
            catch (Exception e)
            {
                LOGGER.Debug("Cannot get registry value " + ((subKeyName != null && subKeyName.Length > 0) ? keyName + "\\" + subKeyName : keyName) + " - " + optionName,e);
                return defaultValue;
            }
        }

        public void setBooleanOption(string subKeyName, string optionName, bool optionValue)
        {
            int value;
            if (optionValue)
            {
                value = 0;
            }
            else
            {
                value = 1;
            }
            Registry.SetValue((subKeyName != null && subKeyName.Length > 0) ? keyName + "\\" + subKeyName : keyName, optionName, value);
        }

        public bool getBooleanOption(string subKeyName, string optionName, bool defaultValue = false)
        {
            try
            {
                int defaultIntValue = defaultValue ? 0 : 1;
                int tInteger = (int)Registry.GetValue((subKeyName != null && subKeyName.Length > 0) ? keyName + "\\" + subKeyName : keyName, optionName, defaultIntValue);
                if (tInteger == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                LOGGER.Debug("Cannot get registry value " + ((subKeyName != null && subKeyName.Length > 0) ? keyName + "\\" + subKeyName : keyName) + " - " + optionName,e);
                return defaultValue;
            }
        }

        public void setIntOption(string subKeyName, string optionName, int optionValue)
        {
            Registry.SetValue((subKeyName != null && subKeyName.Length > 0) ? keyName + "\\" + subKeyName : keyName, optionName, optionValue);
        }

        public int getIntOption(string subKeyName, string optionName, int defaultValue = 0)
        {
            try
            {
                return (int)Registry.GetValue((subKeyName != null && subKeyName.Length > 0) ? keyName + "\\" + subKeyName : keyName, optionName, defaultValue);
            }

            catch (Exception e)
            {
                LOGGER.Debug("Cannot get registry value " + keyName + " - " + optionName,e);
                return defaultValue;
            }
        }
    }
}
