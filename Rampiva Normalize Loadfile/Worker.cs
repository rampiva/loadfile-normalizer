﻿using Rampiva.LoadfileNormalizer.NormalizerModules;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZetaLongPaths;

namespace Rampiva.LoadfileNormalizer
{
    public class Worker
    {
        private static readonly log4net.ILog LOGGER = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        List<NormalizationModule> loadfileNormalizers;
        string sourceLoadfile;
        string destinationLoadfile;
        bool abortWork;
        char loadfileSeparator = '\u0014';
        char loadfileQuotes;

        public bool ErrorsEncountered { set; get; }
        public long LoadfileTotalSize { set; get; }
        public long LoadfileProgressSize { set; get; }

        public long LoadfileTotalLines { set; get; }
        public long LoadfileChangedLines { set; get; }


        public Worker(List<NormalizationModule> loadfileNormalizers, string sourceLoadfile, string destinationLoadfile)
        {
            this.loadfileNormalizers = loadfileNormalizers;
            this.sourceLoadfile = sourceLoadfile;
            this.destinationLoadfile = destinationLoadfile;
        }

        public void Abort()
        {
            abortWork = true;
        }

        public void DoWork()
        {
            try
            {
                abortWork = false;
                ZlpFileInfo sourceLoadfileFI = new ZlpFileInfo(sourceLoadfile);
                LoadfileTotalSize = sourceLoadfileFI.Length;
                if (!sourceLoadfileFI.Exists)
                {
                    throw new Exception("Source loadfile " + sourceLoadfileFI.FullName + " does not exist");
                }

                ZlpFileInfo destinationLoadfileFI = new ZlpFileInfo(destinationLoadfile);
                if (destinationLoadfileFI.Exists)
                {
                    throw new Exception("Destination loadfile " + destinationLoadfileFI.FullName + " already exists");
                }


                TextWriter tw = new StreamWriter(destinationLoadfileFI.OpenWrite(), Encoding.UTF8);
                TextReader tr = new StreamReader(sourceLoadfileFI.OpenRead());

                foreach (var normalizer in loadfileNormalizers)
                {
                    normalizer.setLoadFileFolder(sourceLoadfileFI.Directory);
                }


                long lineId = 0;

                while (true)
                {
                    if (abortWork)
                    {
                        break;
                    }

                    var line = tr.ReadLine();
                    var originalLine = line;

                    // Check end of line
                    if (line == null)
                    {
                        break;
                    }
                    

                    var lineElements = parseLine(line, lineId == 0);

                    if (lineId == 0)
                    {
                        foreach (var normalizer in loadfileNormalizers)
                        {
                            lineElements = normalizer.NormalizeHeader(lineElements);
                        }
                    }
                    else
                    {
                        foreach (var normalizer in loadfileNormalizers)
                        {
                            lineElements = normalizer.NormalizeLine(lineElements, lineId);
                        }
                    }

                    var normalizedLine = buildLine(lineElements);
                    if (normalizedLine != originalLine)
                    {
                        LoadfileChangedLines++;
                    }


                    tw.WriteLine(normalizedLine);

                    lineId++;
                    LoadfileTotalLines++;

                    // Update progress with size of line + 1 for newline
                    LoadfileProgressSize += line.Length + 1;
                }

                tr.Close();
                tw.Close();


                if (abortWork)
                {
                    ErrorsEncountered = true;
                    LOGGER.Info("Normalization stopped by user");
                    destinationLoadfileFI.SafeDelete();
                }
                else
                {
                    LoadfileProgressSize = LoadfileTotalSize;
                    LOGGER.Info("Read " + LoadfileTotalLines + " loadfile lines from " + sourceLoadfileFI.FullName);
                    LOGGER.Info("Wrote " + LoadfileTotalLines + " loadfile lines to " + destinationLoadfileFI.FullName + ", of which");
                    LOGGER.Info("Changed " + LoadfileChangedLines + " lines");

                    foreach (var normalizer in loadfileNormalizers)
                    {
                        LOGGER.Info(normalizer.GetFinishedStatus());
                    }

                }
            }
            catch (Exception e)
            {
                ErrorsEncountered = true;
                LOGGER.Error("Failed normalizing loadfile, ", e);
            }

        }

        private List<string> parseLine(string line, bool header = false)
        {
            // Split line by separator
            var splits = line.Split(new char[] { loadfileSeparator });

            // Reading header
            if (header)
            {
                // Get quotes character
                loadfileQuotes = splits[0][0];
            }

            List<string> lineElements = new List<string>();
            foreach (var split in splits)
            {
                lineElements.Add(split.Trim(loadfileQuotes));
            }
            return lineElements;
        }


        private string buildLine(List<string> elements)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var element in elements)
            {
                if (sb.Length > 0)
                {
                    sb.Append(loadfileSeparator);
                }
                sb.Append(loadfileQuotes);
                sb.Append(element);
                sb.Append(loadfileQuotes);
            }

            return sb.ToString();
        }   
    }
}
